package com.example.apppemesananmami

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_set_location.*
import mumayank.com.airlocationlibrary.AirLocation

class setLocation : AppCompatActivity(), OnMapReadyCallback, View.OnClickListener {
    var airLoc : AirLocation? = null
    var gMap : GoogleMap? = null
    lateinit var mapFragment : SupportMapFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_location)
        mapFragment = supportFragmentManager.findFragmentById(R.id.fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fab.setOnClickListener(this)
        btKembali.setOnClickListener {
            startActivity(Intent(this,CostumerDashboard::class.java))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        airLoc?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        airLoc?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    var lat = -7.894926
    var lang = 111.9857027

    override fun onMapReady(p0: GoogleMap?) {
        gMap = p0
        if (gMap!=null){
            airLoc = AirLocation(this,true, true,
                object : AirLocation.Callbacks{
                    override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                        Toast.makeText(this@setLocation, "Gagal mendapatkan posisi saat ini",
                            Toast.LENGTH_LONG).show()
                        edPosisi.setText("Gagal mendapatkan posisi saat ini")
                    }

                    override fun onSuccess(location: Location) {
                        val ll = LatLng(lat,lang)
                        gMap!!.addMarker(MarkerOptions().position(ll).title("Posisi saya"))
                        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                        edPosisi.setText("Posisi saya : LAT=${lat}, " +
                                "LNG=${lang}")
                    }
                })
        }
    }

    override fun onClick(v: View?) {
        airLoc = AirLocation(this,true, true,
            object : AirLocation.Callbacks{
                override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                    Toast.makeText(this@setLocation, "Gagal mendapatkan posisi saat ini",
                        Toast.LENGTH_LONG).show()
                    edPosisi.setText("Gagal mendapatkan posisi saat ini")
                }

                override fun onSuccess(location: Location) {
                    val ll = LatLng(lat,lang)
                    gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll,16.0f))
                    edPosisi.setText("Posisi saya : LAT=${lat}, " +
                            "LNG=${lang}")
                }
            })
    }
}