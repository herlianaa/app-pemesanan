package com.example.apppemesananmami

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_register.*

class Register : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        btnRegister.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        var regEmail = edRegEmail.text.toString()
        var regPassword = edRegPassword.text.toString()

        if (regEmail.isEmpty() || regPassword.isEmpty()){
            Toast.makeText(this,"Email and password can't be empty!", Toast.LENGTH_LONG).show()
        }
        else if (!cbSyarat.isChecked){
            Toast.makeText(this,"Anda belum menyetujui Terms and Condition!", Toast.LENGTH_LONG).show()
        }
        else{
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("Processing... It won't take a long!")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(regEmail,regPassword)
                .addOnCompleteListener {
                    progressDialog.hide()
                    if (!it.isSuccessful) return@addOnCompleteListener
                    Toast.makeText(this,"Registered Sucessfully!", Toast.LENGTH_LONG).show()
                    clearText()
                    val intent = Intent(this,Login::class.java)
                    startActivity(intent)
                }
                .addOnFailureListener {
                    progressDialog.hide()
                    Toast.makeText(this,"Failed to register!", Toast.LENGTH_LONG).show()
                }
            }
        }
        fun clearText(){
            edRegEmail.setText("")
            edRegPassword.setText("")
        }
    }