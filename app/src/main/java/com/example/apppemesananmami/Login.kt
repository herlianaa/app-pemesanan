package com.example.apppemesananmami

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        txRegister.setOnClickListener {
            startActivity(Intent(this,Register::class.java))
        }
        btnLogin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogin -> {
                var email = edEmail.text.toString()
                var password = edPassword.text.toString()
                if (email.isEmpty() || password.isEmpty()){
                    Toast.makeText(this,"Email dan password kosong!", Toast.LENGTH_LONG).show()
                } else {
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authentificating")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
                        .addOnCompleteListener {
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                            Toast.makeText(this,"Login berhasil", Toast.LENGTH_LONG).show()
                            if (email == "admin@gmail.com"){
                                val intent = Intent(this,AdminDashboard::class.java)
                                startActivity(intent)
                            } else {
                                val intent = Intent(this,CostumerDashboard::class.java)
                                startActivity(intent)
                            }
                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(this,"Email dan password Anda salah!", Toast.LENGTH_LONG).show()
                        }
                }
            }
        }
    }
}