package com.example.apppemesananmami

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_pesanan.*

class Pesanan : AppCompatActivity(),View.OnClickListener {

    val COLLECTION = "pesanan"
    val F_KO = "kodepesanan"
    val F_PRODUK = "produkdipesan"
    val F_HARGAP = "hargapesanan"
    val F_PEMESAN = "pemesan"
    var docId = ""
    lateinit var db : FirebaseFirestore
    lateinit var alPesanan : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter
    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pesanan)
        alPesanan = ArrayList()
        btLogoff.setOnClickListener {
            fbAuth.signOut()
            startActivity(Intent(this,Login::class.java))
        }
        btProduk.setOnClickListener {
            startActivity(Intent(this,AdminDashboard::class.java))
        }
        btDeletePesanan.setOnClickListener (this)
        lsPesanan.setOnItemClickListener(itemClick)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("firestore",e.message)
            showData()
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alPesanan.get(position)
        docId = hm.get(F_KO).toString()
        edKodePesanan.setText(docId)
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alPesanan.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_KO,doc.get(F_KO).toString())
                hm.set(F_PRODUK,doc.get(F_PRODUK).toString())
                hm.set(F_HARGAP,doc.get(F_HARGAP).toString())
                hm.set(F_PEMESAN,doc.get(F_PEMESAN).toString())
                alPesanan.add(hm)
            }
            adapter = SimpleAdapter(this,alPesanan,R.layout.row_data_pesanan,
                arrayOf(F_KO,F_PRODUK,F_HARGAP,F_PEMESAN),
                intArrayOf(R.id.txKodePesanan,R.id.txProdukDibeli,R.id.txJmlPesanan,R.id.txTotal,R.id.txPemesan))
            lsPesanan.adapter = adapter
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btDeletePesanan ->{
                db.collection(COLLECTION).whereEqualTo(F_KO,docId).get().addOnSuccessListener {
                        results ->
                    for (doc in results){
                        db.collection(COLLECTION).document(doc.id).delete()
                            .addOnSuccessListener {
                                Toast.makeText(this, "Data sukses dihapus",
                                    Toast.LENGTH_SHORT).show()
                            }.addOnFailureListener { e ->
                                Toast.makeText(this, "Data gagal dihapus ${e.message}",
                                    Toast.LENGTH_SHORT).show()
                            }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Data gagal dihapus ${e.message}",
                        Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}
