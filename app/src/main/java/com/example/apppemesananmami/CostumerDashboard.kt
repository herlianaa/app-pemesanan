package com.example.apppemesananmami

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_costumer_dashboard.*

class CostumerDashboard : AppCompatActivity(), View.OnClickListener {
    val COLLECTION = "produk"
    val F_KP = "kodeproduk"
    val F_NAMA = "namaproduk"
    val F_JENIS = "jenisproduk"
    val F_HARGA = "harga"

    val COLLECTION2 = "pesanan"
    val F_PDPesan = "produkdipesan"
    val F_KDP = "kodepesanan"
    val F_HPPesan = "hargapesanan"
    val F_NAMAPEMESAN = "pemesan"

    var docId = ""
    lateinit var db : FirebaseFirestore
    lateinit var alProduk : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter
    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_costumer_dashboard)
        lsDataProduk.setOnItemClickListener(itemClick)
        btPesanS.setOnClickListener (this)
        alProduk = ArrayList()
        btLogoff2.setOnClickListener {
            fbAuth.signOut()
            startActivity(Intent(this,Login::class.java))
        }
        btLokasi.setOnClickListener {
            startActivity(Intent(this,setLocation::class.java))
        }
    }
    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("firestore",e.message)
            showData()
        }
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alProduk.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_KDP,doc.get(F_KDP).toString())
                hm.set(F_NAMA,doc.get(F_NAMA).toString())
                hm.set(F_JENIS,doc.get(F_JENIS).toString())
                hm.set(F_HARGA,doc.get(F_HARGA).toString())
                alProduk.add(hm)
            }
            adapter = SimpleAdapter(this,alProduk,R.layout.row_data,
                arrayOf(F_KP,F_NAMA,F_JENIS,F_HARGA),
                intArrayOf(R.id.txKp,R.id.txNamaproduk,R.id.txJenis,R.id.txHarga))
            lsDataProduk.adapter = adapter
        }
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alProduk.get(position)
        edPesanProduk.setText(hm.get(F_NAMA).toString())
        edPesanHarga.setText(hm.get(F_HARGA).toString())
    }

    fun clearField(){
        edPesanHarga.setText("")
        edPesanPemesan.setText("")
        edPesanProduk.setText("")
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btPesanS -> {
                val hm = HashMap<String, Any>()
                hm.set(F_NAMAPEMESAN,edPesanPemesan.text.toString())
                hm.set(F_HPPesan,edPesanHarga.text.toString())
                hm.set(F_PDPesan,edPesanProduk.text.toString())
                db.collection(COLLECTION2).add(hm).
                addOnSuccessListener {
                    Toast.makeText(this,"Data Sukses Ditambahkan", Toast.LENGTH_SHORT)
                        .show()
                    clearField()
                }.addOnFailureListener { e ->
                    Toast.makeText(this,
                        "Data Gagal Ditambahkan : ${e.message}", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }
}
