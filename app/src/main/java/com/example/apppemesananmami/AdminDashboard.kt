package com.example.apppemesananmami

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_admin_dashboard.*

class AdminDashboard : AppCompatActivity() , View.OnClickListener {

    val COLLECTION = "produk"
    val F_KP = "kodeproduk"
    val F_NAMA = "namaproduk"
    val F_JENIS = "jenisproduk"
    val F_HARGA = "harga"
    var docId = ""
    lateinit var db : FirebaseFirestore
    lateinit var alProduk : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter
    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_dashboard)
        btLogoff.setOnClickListener {
            fbAuth.signOut()
            startActivity(Intent(this,Login::class.java))
        }
        alProduk = ArrayList()
        btInsert.setOnClickListener(this)
        btUpdate.setOnClickListener(this)
        btDelete.setOnClickListener(this)
        btPesanan.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClick)

    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("firestore",e.message)
            showData()
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btInsert ->{
                val hm = HashMap<String, Any>()
                hm.set(F_KP,edKode.text.toString())
                hm.set(F_NAMA,edNamaproduk.text.toString())
                hm.set(F_JENIS,edJenis.text.toString())
                hm.set(F_HARGA,edHarga.text.toString())
                db.collection(COLLECTION).document(edKode.text.toString()).set(hm).
                addOnSuccessListener {
                    Toast.makeText(this,"Data Sukses Ditambahkan", Toast.LENGTH_SHORT)
                        .show()
                }.addOnFailureListener { e ->
                    Toast.makeText(this,
                        "Data Gagal Ditambahkan : ${e.message}", Toast.LENGTH_SHORT)
                        .show()
                }
            }
            R.id.btUpdate ->{
                val hm = HashMap<String, Any>()
                hm.set(F_KP,docId)
                hm.set(F_NAMA,edNamaproduk.text.toString())
                hm.set(F_JENIS,edJenis.text.toString())
                hm.set(F_HARGA,edHarga.text.toString())
                db.collection(COLLECTION).document(docId).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this, "Data Sukses Diedit", Toast.LENGTH_SHORT)
                            .show() }
                    .addOnFailureListener { e ->
                        Toast.makeText(this,
                            "Data Gagal Diedit : ${e.message}", Toast.LENGTH_SHORT)
                            .show()
                    }
            }
            R.id.btDelete ->{
                db.collection(COLLECTION).whereEqualTo(F_KP,docId).get().addOnSuccessListener {
                        results ->
                    for (doc in results){
                        db.collection(COLLECTION).document(doc.id).delete()
                            .addOnSuccessListener {
                                Toast.makeText(this, "Data sukses dihapus",
                                    Toast.LENGTH_SHORT).show()
                            }.addOnFailureListener { e ->
                                Toast.makeText(this, "Data gagal dihapus ${e.message}",
                                    Toast.LENGTH_SHORT).show()
                            }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Data gagal dihapus ${e.message}",
                        Toast.LENGTH_SHORT).show()
                }
            }

            R.id.btPesanan ->{
                val intent = Intent(this,Pesanan::class.java)
                startActivity(intent)
            }
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alProduk.get(position)
        docId = hm.get(F_KP).toString()
        edKode.setText(docId)
        edNamaproduk.setText(hm.get(F_NAMA).toString())
        edJenis.setText(hm.get(F_JENIS).toString())
        edHarga.setText(hm.get(F_HARGA).toString())

    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alProduk.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_KP,doc.get(F_KP).toString())
                hm.set(F_NAMA,doc.get(F_NAMA).toString())
                hm.set(F_JENIS,doc.get(F_JENIS).toString())
                hm.set(F_HARGA,doc.get(F_HARGA).toString())
                alProduk.add(hm)
            }
            adapter = SimpleAdapter(this,alProduk,R.layout.row_data,
                arrayOf(F_KP,F_NAMA,F_JENIS,F_HARGA),
                intArrayOf(R.id.txKp,R.id.txNamaproduk,R.id.txJenis,R.id.txHarga))
            lsData.adapter = adapter
        }
    }

}
